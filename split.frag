#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 r = u_resolution;
float t = u_time;

void main(){
		vec2 uv = (gl_FragCoord.xy - .5 * r) / r.y;
		vec2 st = uv;
		uv *= fract(t) * 500.;
		uv = fract(uv);
		st += floor(t);
		st = fract(st);
		float  circle = smoothstep(0.1,0.15,distance(uv,vec2(0.5)) * distance(st,vec2(0.5)));
		

		gl_FragColor = vec4(circle);
}
