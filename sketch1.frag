#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 res = u_resolution;
float t = u_time;

vec3 rgb2hsl(vec3 color);
 vec3 hsl2rgb( in vec3 c );

void main(){
	vec2 uv = (gl_FragCoord.xy-.5*u_resolution.xy) / u_resolution.y;
	vec2 st = uv;
	uv *=2.;
	vec2 uv2 = uv;
	vec2 uv3 = uv;

	uv += vec2(cos(t*0.3887),sin(t*0.19)) * (1.5*sin(t*0.1));
	uv2 += vec2(cos(t*0.289),sin(t*0.427)) * (1.6*sin(t*0.08));
	uv3 += vec2(cos(t*0.189),sin(t*0.327)) * (1.5*cos(t*0.16));

	float dist = length(uv);
	float dist2 = length(uv2);
	float dist3 = length(uv3);

	//float c = smoothstep(0.,0.3,fract(dist));
	float c = smoothstep(0.2,0.25, fract(dist));
	float c2 = smoothstep(0.2,0.25, fract(dist2));
	float c3 = smoothstep(0.2,0.25, fract(dist3));

	vec3 col = hsl2rgb(vec3(mix(sin(t*0.02)+cos(t*0.18),sin(t*0.23),fract(dist+t*0.1)),0.2,0.5));
	vec3 col2 = hsl2rgb(vec3(mix(cos(t*0.38),sin(t*0.23)+cos(t*0.77),fract(dist2-t*0.2)),sin(t)*0.2,0.5));
	vec3 col3 = hsl2rgb(vec3(mix(cos(t*0.178),sin(t*0.123),fract(dist3)),sin(t)*0.3,0.2));

	col -= col2 -col3;
	col -= length(st*0.5);
	col = rgb2hsl(vec3(col.g,col.r,col.b));
	col = hsl2rgb(vec3(col.b,col.g,col.r));


	gl_FragColor = vec4(col.r,col.g,col.b,1);
}

// METHODS

vec3 rgb2hsl(vec3 color) {
 	vec3 hsl; // init to 0 to avoid warnings ? (and reverse if + remove first part)

 	float fmin = min(min(color.r, color.g), color.b); //Min. value of RGB
 	float fmax = max(max(color.r, color.g), color.b); //Max. value of RGB
 	float delta = fmax - fmin; //Delta RGB value

 	hsl.z = (fmax + fmin) / 2.0; // Luminance

 	if (delta == 0.0) //This is a gray, no chroma...
 	{
 		hsl.x = 0.0; // Hue
 		hsl.y = 0.0; // Saturation
 	} else //Chromatic data...
 	{
 		if (hsl.z < 0.5)
 			hsl.y = delta / (fmax + fmin); // Saturation
 		else
 			hsl.y = delta / (2.0 - fmax - fmin); // Saturation

 		float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;
 		float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;
 		float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;

 		if (color.r == fmax)
 			hsl.x = deltaB - deltaG; // Hue
 		else if (color.g == fmax)
 			hsl.x = (1.0 / 3.0) + deltaR - deltaB; // Hue
 		else if (color.b == fmax)
 			hsl.x = (2.0 / 3.0) + deltaG - deltaR; // Hue

 		if (hsl.x < 0.0)
 			hsl.x += 1.0; // Hue
 		else if (hsl.x > 1.0)
 			hsl.x -= 1.0; // Hue
 	}

 	return hsl;
 }

 vec3 hsl2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

    return c.z + c.y * (rgb-0.5)*(1.0-abs(2.0*c.z-1.0));
}