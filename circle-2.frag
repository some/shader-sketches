uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#define NUM_LAYERS 1./3.

void main(){
		vec2 uv = (gl_FragCoord.xy-.5*u_resolution.xy) / u_resolution.y;
		vec3 col = vec3(0.);
		float m=0.;

		
		for(float i=0.; i<1. ; i+=NUM_LAYERS){
				float depth = fract(i-u_time/50.);

		vec2 gv = fract(uv  * vec2(10.*depth))-.5-(u_mouse/u_resolution);

			for(float y=-1.;y<=1.;y++){
				for(float x=-1.; x<=1.;x++){
						vec2 offs = vec2(x, y);
						float d = length(gv+offs);
						float r = 0.03*(1.-depth*depth); 
						m += 1.- step(r, d);
				}
			}

		}

		col += m;
		gl_FragColor = vec4(col,1.);


}
