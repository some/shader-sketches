#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 res = u_resolution;
float t = u_time;
 vec3 hsl2rgb( in vec3 c );

float bullseye(vec2 uv, float numRings, float pos, float w);

void main(){
	vec2 uv = (gl_FragCoord.xy-0.5*res) / res.y;
	vec2 uvx = uv;

	uv.x += sin(t*0.1) * 0.2;
	// uv *= 30;
	// float d = length(uv);
	// float outer = smoothstep(0.3,0.35,fract(d));
	// float inner = smoothstep(0.3,0.25,fract(d));
	// outer += inner;
	float nR = 5;
	float t1 = bullseye(uvx,nR,0.5,0.3);
	float t2 = bullseye(uvx+vec2(0.01*sin(t),0.01),nR,0.5,0.3);
	float t3 = bullseye(uvx+vec2(-0.01,-0.01),nR,0.5,0.3);
	vec3 tColor = vec3(t2,t3,0.5);
	//tColor += t1;
	tColor *= vec3(1.,1.,0.5);
	tColor = hsl2rgb(tColor);
	
	float b1 = bullseye(uv,nR,0.5,0.3);
	float b2 = bullseye(uv+vec2(0.01*sin(t),0.01),nR,0.5,0.3);
	float b3 = bullseye(uv+vec2(-0.01,-0.01),nR,0.5,0.3);
	vec3 outColor = vec3(b2,b3,b2);
	outColor += b1;
	outColor = outColor - tColor;
	outColor = hsl2rgb(outColor);

	gl_FragColor = vec4(outColor,1.);
}

float bullseye(vec2 uv, float numRings, float pos, float w)
{
	uv *= numRings;
	float d = length(uv);
	float outer = smoothstep(pos, pos+(w*0.5), fract(d));
	float inner = smoothstep(pos, pos-(w*0.5), fract(d));
	return outer + inner;

}


 vec3 hsl2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

    return c.z + c.y * (rgb-0.5)*(1.0-abs(2.0*c.z-1.0)) ;
}