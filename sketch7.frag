#version 120

vec3 palette(float t,vec3 a, vec3 b, vec3 c, vec3 d){
		return a + b*cos(6.28318*(c*t+d));
}
float N21(vec2 p) {
	return fract(sin(p.x*12293.83019+p.y*3450396.3881)*3903524.03919);
}

vec2 N22(vec2 p) {
	return vec2(N21(p), N21(p+3249.133));
}

uniform vec2 u_resolution;
uniform float u_time;
float t = u_time;

void main(){
		vec3 a = vec3(.5,.5,.5);
		vec3 b = vec3(.5,.5,.5);
		vec3 c = vec3(1.,1.,1.);
	
		vec3 d = vec3(.0,.1,.2);
		vec2 uv = (2. * gl_FragCoord.xy - u_resolution) / u_resolution.y;
		uv *= 1.5;

		vec3 col = palette(uv.x*.5,a,b,c,d);

		float balls;
		float minDist = 100;
		for(float i = 0.; i < 3.; i++){
				vec2 ball = N22(vec2(sin(i)));
				vec2 p = sin(ball * u_time ) * 1.0;
				p *= sin(N22(uv*10*sin(u_time*0.01)  * 0.1 * sin(u_time * 0.1)));
				float d = length(uv - p);
				//float d = length(uv + N22(uv)-p );
				balls += smoothstep(0.02,0.01,d);
				if( d < minDist){
					minDist = d;
				}
		}
		col = vec3(balls);
		col = vec3(minDist);
		minDist = clamp(minDist,0,1);
		col = palette(minDist + fract(t*0),a,b,c,d);


		gl_FragColor = vec4(col,1.);
}
