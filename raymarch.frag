#version 120


uniform vec2 u_resolution;
uniform float u_time;

vec2 r = u_resolution;
float t = u_time;

#define MAX_STEPS 100
#define MAX_DIST 100.
#define SURF_DIST .01


float RayMarch(vec3 ro, vec3 rd);
float getDist(vec3 p);
float getLight(vec3 p);
vec3 getNormal(vec3 p);

void main(){
		vec2 uv = (gl_FragCoord.xy - .5*r.xy)/r.y;

		vec3 rayOrigin = vec3(0,1,0);
		vec3 rayDirection = normalize(vec3(uv.x,uv.y,1.));

		float d = RayMarch(rayOrigin,rayDirection);
		vec3 p = rayOrigin + rayDirection * d;
		float diffused = getLight(p);



		vec3 col = vec3(diffused);

		gl_FragColor = vec4(col,1.);
}



float RayMarch(vec3 ro, vec3 rd){
		//keep track of distance origin
		float distanceOrigin = 0.;

		for(int i = 0; i<MAX_STEPS; i++){
			vec3 p = ro + rd*distanceOrigin;
			float distanceToScene = getDist(p);
			distanceOrigin += distanceToScene; 
			if(distanceOrigin>MAX_DIST || distanceToScene<SURF_DIST) break;
		}

		
		return distanceOrigin;

}

float getDist(vec3 p){
		vec4 sphere = vec4(0,1,6,1); // pos.xyz and radius

		float sphereDist = length(p - sphere.xyz) - sphere.w; 
		float planeDist = p.y; 

		float totalDistance = min(sphereDist, planeDist);
		return totalDistance;
}

float getLight(vec3 p){
		vec3 lightPos = vec3(0, 5, 6); 
		lightPos.xz += vec2(sin(t),cos(t))*2.;
		vec3 l = normalize(lightPos-p);
		vec3 n = getNormal(p);
		float dif = clamp(dot(n, l),0.,1.);
		float d = RayMarch(p+n*SURF_DIST*2.,l);
		if(d<length(lightPos-p)) dif *= .1;
		return dif;
}

vec3 getNormal(vec3 p){
		float d = getDist(p);
		vec2 e = vec2(.01,0);
		vec3 n = d - vec3(
						getDist(p-e.xyy),
						getDist(p-e.yxy),
						getDist(p-e.yyx));
		return normalize(n);
}
