#version 120

uniform float u_time;
uniform vec2 u_resolution;

float t  = u_time;
vec2 r  = u_resolution;

//I don't know what this constant does...
#define D .6

float wave(vec2 p){
		float v = sin(p.x + cos(p.y) + sin(p.y*.23));
		return v*v;
}
const mat2 rot = mat2(.5,.86,-.86,.5);

float map(vec2 p){
		float v = 0.;
		for(int i = 0; i< 2; i++){
				v+= wave(p);
				p.x += t;
				p *= rot;
		}
		//v += wave(p);
		v= abs(1.5-v);
		return v;
}

void main(){
		// make uv coordinates go from -1 to 1 
		vec2 uv = (gl_FragCoord.xy*2. - r.xy)/r.y;
		//get 3d direction of each uv 
		//convert to vec2 and scale to 19
		vec2 p = normalize(vec3(uv,2.3)).xy * 19.; 
		//increment p.y with time
		p.y += t*2.;
		float v = map(p);

		vec3 c = mix( vec3(0.0,.4,.5), vec3(1.,.3+map(p*2.5)*.2, .5), v);
		vec3 n = normalize(vec3(v-map(vec2(p.x+D,p.y)), v-map(vec2(p.x,p.y+D)),-D));
		vec3 l = normalize(vec3(.5,.5,-1.0));

		v = dot(l,n) + pow(dot(l,n), 180.);
		c.rg *= v;


		gl_FragColor = vec4(c,1.);
}
