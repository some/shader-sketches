#version 120

uniform vec2 u_resolution;
uniform float u_time;
mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r; } 
float snoise(vec3 v);
float fbm(vec3 x) {
	float v = 0.0;
	float a = 1.0;
	vec3 shift = vec3(u_time*0.001);
	for (int i = 0; i <1; ++i) {
		v += a * snoise(x+vec3(cos(i+100+u_time*0.01),sin(i*10+u_time*0.01),u_time*0.01));
		x.xy *= rotate2d(sin(u_time*0.000000001) * 360.);
		x = x * 2.0 + shift;
		a *= 0.5;
	}
	return v;
}
vec3 palette( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )
{
    return a + b*cos( 6.28318*(c*t+d) );
}

void main(){
	vec3 a = vec3(.5,.5,.5);
		vec3 b = vec3(.5,.5,.5);
		vec3 c = vec3(1.,1.,1.);
	
		vec3 d = vec3(.0,.1,.2);
	vec2 uv = (gl_FragCoord.xy - .5 * u_resolution) / u_resolution.y;
	float t = u_time;
	t *= 0.01;
	uv *= 1.1;
	float val = fbm(vec3(uv,t));
	val = mod(val, 0.5);
	val = smoothstep(0.00,0.25,val) - smoothstep(0.250,0.500,val);
	val = smoothstep(0.,1.,val);
	vec3 col;
	col = palette(val+t,a,b,c,d);

	float val2 = fbm(vec3(uv*rotate2d(240)+vec2(0.0005,0.0),t+4991));
	val2 = mod(val2, 0.5);
	val2 = smoothstep(0.00,0.25,val2) - smoothstep(0.250,0.500,val2);
	val2 = smoothstep(0.,1.,val2);
	col.r *= palette(val2+t,a,b,c,d).r;

	float val3 = fbm(vec3(uv*rotate2d(120)+vec2(-0.01,0.0005),t+10494));
	val3 = mod(val3, 0.5);
	val3 = smoothstep(0.00,0.25,val3) - smoothstep(0.250,0.500,val3);
	val3 = smoothstep(0.,1.,val3);
	col.g += palette(val3+t,a,b,c,d).g;





	gl_FragColor = vec4(col, 1.);

}

//	Simplex 3D Noise 
//	by Ian McEwan, Ashima Arts

float snoise(vec3 v){ 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //  x0 = x0 - 0. + 0.0 * C 
  vec3 x1 = x0 - i1 + 1.0 * C.xxx;
  vec3 x2 = x0 - i2 + 2.0 * C.xxx;
  vec3 x3 = x0 - 1. + 3.0 * C.xxx;

// Permutations
  i = mod(i, 289.0 ); 
  vec4 p = permute( permute( permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients
// ( N*N points uniformly over a square, mapped onto an octahedron.)
  float n_ = 1.0/7.0; // N=7
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z *ns.z);  //  mod(p,N*N)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}