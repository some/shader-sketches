#version 120

uniform vec2 u_resolution;
uniform float u_time;

float t = u_time;

float ball(vec2 p){
		return distance(p,vec2(sin(t),cos(t))*0.3) * 5. ;
}

void main(){
		vec2 uv = (gl_FragCoord.xy-0.5*u_resolution)/u_resolution.y;
		
		float ball2 = distance(uv,vec2(0.,sin(t))*0.2) * 6.;
		vec2 gOffset = vec2(0.1,0.0);
		vec2 bOffset = vec2(-0.1,0.);
		vec3 col = vec3(ball(uv),ball(uv+gOffset),ball(uv+bOffset));
		col *= ball2;
		gl_FragColor = vec4(col,1.);
}
