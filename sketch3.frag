#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 res = u_resolution;
float t = u_time;


void main(){
	vec2 uv = (gl_FragCoord.xy-0.5*res) / res.y;
	uv *= 5;
	float ball = distance(uv,vec2(0.));

	vec2 ball2loc = vec2(1.5*sin(t*0.314+uv.y)*cos(t*0.413),0.8);
	float ball2 = distance(uv,ball2loc);

	vec2 ball3loc = vec2(1.,cos(t*0.223)*1.6);
	float ball3 = distance(uv,ball3loc);
	ball3 /= 2;

	vec2 ball4loc = vec2(0.4*cos(t*0.152),0.31*sin(t*0.093));
	float ball4 = distance(uv,ball4loc);
	//ball2 *= ball3;
	ball *= ball2;
	float bb = ball;
	ball *= ball3;
	float bbb = ball;
	ball -= ball4;
	float bbbb = ball;
	ball *= bb;
	ball *= bbb;
	ball *= bbbb;

	vec3 col = vec3(1.-ball+(uv.x*0.2),ball+(uv.y*0.2),ball);
	col = 1.-col;
	col.r *= -1;
	col.g *= 0.2;
	col.b *= 0.3;
	gl_FragColor = vec4(col,1.);

}