#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 r = u_resolution;
float t = u_time;

float easeInQuad(float t){
		return  t * t;
}

float easeOutQuad(float t){
		return -1.0 * t * (t - 2.);
}

float easeInOutQuad(float t){
		if(( t*= 2.0) < 1.0){
				return 0.5 * t * t;
		} else {
				return -0.5 * (( t - 1.0) * (t - 3.0) - 1.0);
		}
}

float easeInCubic(float t){
		return t * t * t;
}

float easeInExpo(float t){
		return ( t == 0.0) ? 0.0 : pow(2., 10. * (t-1.0));
}

float stepUpDown(float begin, float end, float t){
		return step(begin, t) - step(end, t);
}

void main(){
		vec2 uv = (gl_FragCoord.xy-.5*r)/r.y;
		float timeStep = mod(floor(t),3); 
		vec2 st = vec2(atan(uv.y,uv.x), length(uv));

		float circle;
		if ( stepUpDown(0.0,1.0,timeStep) == 1){
		circle = step(0.20,st.y) - step(0.21,st.y);
		} else {
			circle = step(0.2,st.y); 
		}




		gl_FragColor = vec4(vec3(1.-circle),1.);

			
}
