#version 120

uniform vec2 u_resolution;
uniform float u_time;
float t = u_time;

void main(){
		vec2 uv = gl_FragCoord.xy / u_resolution;
		//interpolation between 0 and 1 at the given border. 
		// param 1 = lower bound
		// param 2 = higher bound
		// param3 = value to be interpolated 
		float c = smoothstep(0.0,1.,uv.x);
		c = pow(c, abs(5.*atan(t)));
		c = 1. - c;
		c += sin(t)- abs(sin(mod(gl_FragCoord.y*0.5,17.)));

		gl_FragColor = vec4(vec3(c),1.);

}
