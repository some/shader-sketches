uniform vec2 u_resolution;
uniform float u_time;

vec3 color = vec3(0.,0.,0.);

#define radius 0.1 
#define NUM_LAYERS 2.

void main(){
		
		for(float i = 0.; i<1.0;i+=1.0/NUM_LAYERS){
			vec2 uv = (gl_FragCoord.xy - .5 ) * u_resolution / u_resolution.y;
			uv *= vec2(fract(i+u_time/200.));
			uv = fract(uv);
			vec2 dist = uv - vec2(radius);
			float circle = 1.0-step(radius, dot(dist, dist)*8.);
			color += vec3(circle);
		
		}
	

	gl_FragColor = vec4(color,1.);
}
