#version 120

uniform vec2 u_resolution;

void main(){
		vec2 uv = (gl_FragCoord.xy - 0.5 * u_resolution) / u_resolution.y;
		vec2 uv2 = uv;
		uv *= 10.;
		uv = mod(uv,3.);
		uv -= vec2(0.5);
		uv2 += vec2(2.0,0.);

		float circle = length(uv) - 0.1;
		float circle2 = length(uv2) - 0.1;
		circle = smoothstep(0.,1./u_resolution.x,circle);
		circle2 = smoothstep(0.,1./u_resolution.x,circle);


		gl_FragColor = vec4(circle*circle2);
}
