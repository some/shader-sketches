#version 120

uniform vec2 u_resolution;
uniform float u_time;

#define ORANGE vec3(222.,107.,40.)
#define TEAL vec3(67.,134.,150.)

vec3 rgb2xyz( vec3 c ) {
    vec3 tmp;
    tmp.x = ( c.r > 0.04045 ) ? pow( ( c.r + 0.055 ) / 1.055, 2.4 ) : c.r / 12.92;
    tmp.y = ( c.g > 0.04045 ) ? pow( ( c.g + 0.055 ) / 1.055, 2.4 ) : c.g / 12.92,
    tmp.z = ( c.b > 0.04045 ) ? pow( ( c.b + 0.055 ) / 1.055, 2.4 ) : c.b / 12.92;
    return 100.0 * tmp *
        mat3( 0.4124, 0.3576, 0.1805,
              0.2126, 0.7152, 0.0722,
              0.0193, 0.1192, 0.9505 );
}

vec3 xyz2lab( vec3 c ) {
    vec3 n = c / vec3( 95.047, 100, 108.883 );
    vec3 v;
    v.x = ( n.x > 0.008856 ) ? pow( n.x, 1.0 / 3.0 ) : ( 7.787 * n.x ) + ( 16.0 / 116.0 );
    v.y = ( n.y > 0.008856 ) ? pow( n.y, 1.0 / 3.0 ) : ( 7.787 * n.y ) + ( 16.0 / 116.0 );
    v.z = ( n.z > 0.008856 ) ? pow( n.z, 1.0 / 3.0 ) : ( 7.787 * n.z ) + ( 16.0 / 116.0 );
    return vec3(( 116.0 * v.y ) - 16.0, 500.0 * ( v.x - v.y ), 200.0 * ( v.y - v.z ));
}

vec3 rgb2lab(vec3 c) {
    vec3 lab = xyz2lab( rgb2xyz( c ) );
    return vec3( lab.x / 100.0, 0.5 + 0.5 * ( lab.y / 127.0 ), 0.5 + 0.5 * ( lab.z / 127.0 ));
}

vec3 lab2xyz( vec3 c ) {
    float fy = ( c.x + 16.0 ) / 116.0;
    float fx = c.y / 500.0 + fy;
    float fz = fy - c.z / 200.0;
    return vec3(
         95.047 * (( fx > 0.206897 ) ? fx * fx * fx : ( fx - 16.0 / 116.0 ) / 7.787),
        100.000 * (( fy > 0.206897 ) ? fy * fy * fy : ( fy - 16.0 / 116.0 ) / 7.787),
        108.883 * (( fz > 0.206897 ) ? fz * fz * fz : ( fz - 16.0 / 116.0 ) / 7.787)
    );
}

vec3 xyz2rgb( vec3 c ) {
    vec3 v =  c / 100.0 * mat3( 
        3.2406, -1.5372, -0.4986,
        -0.9689, 1.8758, 0.0415,
        0.0557, -0.2040, 1.0570
    );
    vec3 r;
    r.x = ( v.r > 0.0031308 ) ? (( 1.055 * pow( v.r, ( 1.0 / 2.4 ))) - 0.055 ) : 12.92 * v.r;
    r.y = ( v.g > 0.0031308 ) ? (( 1.055 * pow( v.g, ( 1.0 / 2.4 ))) - 0.055 ) : 12.92 * v.g;
    r.z = ( v.b > 0.0031308 ) ? (( 1.055 * pow( v.b, ( 1.0 / 2.4 ))) - 0.055 ) : 12.92 * v.b;
    return r;
}

vec3 lab2rgb(vec3 c) {
    return xyz2rgb( lab2xyz( vec3(100.0 * c.x, 2.0 * 127.0 * (c.y - 0.5), 2.0 * 127.0 * (c.z - 0.5)) ) );
}

vec2 res = u_resolution;
float t = u_time;
vec3 hsl2rgb( in vec3 c );
vec3 rgb2hsl(vec3 color);
const float X = 0.950470;
const float Y = 1.0;
const float Z = 1.088830;

const float t0 = 4.0 / 29.0;
const float t1 = 6.0 / 29.0;
const float t2 = 3.0 * t1 * t1;
const float t3 = t1 * t1 * t1;

float lab_xyz(float t) {
    return t > t1 ? t * t * t : t2 * (t - t0);
}

float xyz_rgb(float x) {
    return x <= 0.0031308 ? 12.92 * x : 1.055 * pow(x, 1.0 / 2.4) - 0.055;
}



vec3 RGB(vec3 color){
	return vec3(color.r/255., color.g/255, color.b/255);
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 color_mix(vec3 a, vec3 b, float t) {
    //return a * (1.0-t)*(1.0-t) + 2 * sqrt(a*b) * t * (1.0-t) + b * t*t;

    float omt = 1.0-t;
    float t2 = t * t;
    vec3 bt2 = b * t2;
    vec3 abt2 = a * bt2;
    return (a * omt + 2.0 * sqrt(abt2)) * omt + bt2;
}
vec3 color_mix2(vec3 a, vec3 b, float t) {
    float omt = 1.0-t;
    return a * omt*omt + b * t*t;
}

float n21(vec2 uv){
	return fract(sin(uv.x*124712084 + uv.y * 123781) * 8319.41238);
}

void main(){
	//vec2 uv = (gl_FragCoord.xy-0.5*res) / res.y;
	vec2 uv = gl_FragCoord.xy / res;
	vec2 uuv = uv;
	 uv.y *= 4;
	// uv.y += u_time * 0.1;
	// uv.y = mod(uv.y,4);
	// uv.x += u_time * 0.1;
	// uv.x = fract(uv.x);
	//uv.x = floor(uv.x*15.)/15.;

	vec3 col;
	if(uv.x > 0.5){
		col = RGB(ORANGE);
	} else {
		col = RGB(TEAL);
	}
	if(floor(uv.y) == 0){
		//col = smoothstep(RGB(TEAL),RGB(ORANGE),vec3(uv.x));
		//col = mix(RGB(TEAL),RGB(ORANGE),vec3(uv.x));
		col = color_mix2(RGB(TEAL),RGB(ORANGE),uv.x);
		col += n21(uv) * 0.05;

		//col = color_mix(RGB(TEAL),RGB(ORANGE),uv.x);
	}
	if(floor(uv.y) == 2){
		vec3 c1 = rgb2hsl(RGB(TEAL));
		vec3 c2 = rgb2hsl(RGB(ORANGE));
		//col = smoothstep(c2,c1,vec3(uv.x));
		//col = mix(c1,c2,vec3(uv.x));
		col = color_mix2(c1,c2,uv.x);
		col += n21(uv) * 0.05;
		col = hsl2rgb(col);

	}
	if(floor(uv.y) == 1){
		vec3 c1 = rgb2lab(RGB(TEAL));
		vec3 c2 = rgb2lab(RGB(ORANGE));
		col = mix(c1,c2,vec3(uv.x));
		col = color_mix2(c1,c2,uv.x);
		col += n21(uv) * 0.15;
		col = lab2rgb(col);
	}
	if(floor(uv.y) == 3){
		vec3 c1 = rgb2hsv(RGB(TEAL));
		vec3 c2 = rgb2hsv(RGB(ORANGE));
		//col = mix(c1,c2,vec3(uv.x));
		col = color_mix2(c1,c2,uv.x);
		col = hsv2rgb(col + (n21(uv) * 0.04) );
	}
	gl_FragColor = vec4(col,1.);

}

//////////////////////////////////////////////
//////////////////////////////////////////////
//////////////////////////////////////////////

vec3 rgb2hsl(vec3 color) {
 	vec3 hsl; // init to 0 to avoid warnings ? (and reverse if + remove first part)

 	float fmin = min(min(color.r, color.g), color.b); //Min. value of RGB
 	float fmax = max(max(color.r, color.g), color.b); //Max. value of RGB
 	float delta = fmax - fmin; //Delta RGB value

 	hsl.z = (fmax + fmin) / 2.0; // Luminance

 	if (delta == 0.0) //This is a gray, no chroma...
 	{
 		hsl.x = 0.0; // Hue
 		hsl.y = 0.0; // Saturation
 	} else //Chromatic data...
 	{
 		if (hsl.z < 0.5)
 			hsl.y = delta / (fmax + fmin); // Saturation
 		else
 			hsl.y = delta / (2.0 - fmax - fmin); // Saturation

 		float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;
 		float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;
 		float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;

 		if (color.r == fmax)
 			hsl.x = deltaB - deltaG; // Hue
 		else if (color.g == fmax)
 			hsl.x = (1.0 / 3.0) + deltaR - deltaB; // Hue
 		else if (color.b == fmax)
 			hsl.x = (2.0 / 3.0) + deltaG - deltaR; // Hue

 		if (hsl.x < 0.0)
 			hsl.x += 1.0; // Hue
 		else if (hsl.x > 1.0)
 			hsl.x -= 1.0; // Hue
 	}

 	return hsl;
 }

 vec3 hsl2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

    return c.z + c.y * (rgb-0.5)*(1.0-abs(2.0*c.z-1.0)) ;
}