// Necip's mod. 07.04.20

// GFX #90
#ifdef GL_ES
precision mediump float;
#endif


uniform float u_time;
uniform vec2 u_resolution;
 

void main( void ) {
	vec2 uv = (gl_FragCoord.xy / u_resolution.xy);
	
	vec2 r  = u_resolution.xy;
	float t = u_time;
	
	float c = fract( sin(uv.x * r.x*0.8) *
			 sin(uv.y * r.y*0.8 )  ); 
	
	uv.x += sin(u_time)*3.0 *((uv.x*uv.y) +  pow(c,20.));
	
	
	gl_FragColor = vec4(c*mod(t+uv.y,4.0), c*mod(t+uv.x,4.0), 1., 1.0 );
}
