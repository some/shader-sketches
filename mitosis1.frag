#version 120

uniform vec2 u_resolution;
uniform float u_time;


vec2 r = u_resolution;
float t = u_time;
mat2 rot(float a){
		return mat2(cos(a),sin(a),-sin(a),cos(a));
}

float cubicEase(float t){
		float e = sin(t) * 0.5 + 0.5;
		return e * e;
}

float circle(vec2 uv, vec2 id){
		float lower;
		lower = abs(sin(t*2.) * sin(t*2.))*0.45; 
		float upper= 0.5;
		float c = smoothstep(lower,upper,distance(uv, id+0.5));
		c *= smoothstep(lower,upper,distance(uv, id+0.5+vec2(1.,0.)));
		c *= smoothstep(lower,upper,distance(uv, id+0.5+vec2(-1.,0.)));
		c *= smoothstep(lower,upper,distance(uv, id+0.5+vec2(0.,1.)));
		c *= smoothstep(lower,upper,distance(uv, id+0.5+vec2(0.,-1.)));
		return c;
}
void main(){
		vec3 col = vec3(1.);

		vec2 uv = (gl_FragCoord.xy -.5 * r) / r.y;
		uv *= 8.+(2.*cubicEase(t));
		uv *= rot(t*0.1);
		uv.x += sin(t*0.1);
		uv.y += cos(t*0.1);

		uv.x += sin(uv.x+t*0.5);
		uv.y += cos(uv.y+t*0.5);
		

		

		vec2 id = floor(uv);

		if(floor(sin(t)) == 0){
			if(mod(id.y, 2) == 0){
				id.x += cos(t) ;
			} else {
				id.x -= cos(t);
			}
		} else {
			if(mod(id.x, 2) == 0){
				id.y += cos(t);
			} else {
				id.y -= cos(t);
			}
		}

		float r = circle(uv,id);
		float g = circle(uv+vec2(0.01),id);
		float b = circle(uv +vec2(-0.01),id);

		col = vec3(g,r,b);



		gl_FragColor = vec4(col,1.);

}
