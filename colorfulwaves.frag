#version 120

uniform float u_time;
uniform vec2 u_resolution;

void main(){
		vec2 position = (gl_FragCoord.xy - 0.5 * u_resolution) / u_resolution.y;
		float longest = sqrt(float(u_resolution.x*u_resolution.x) + float(u_resolution.y*u_resolution.y)) * 0.5;
		float dx = gl_FragCoord.x-u_resolution.x/2.;
		float dy = 0.2+gl_FragCoord.y-u_resolution.y/2.;
		float len = sqrt(dx*dx+dy*dy);
		float ds = len/longest;
		float md = u_time*2.;
		float ang = 2.*atan(dy, (len+dx));
		ang += pow(len, 0.5)*5.;

		float r =(128. - sin(ang + md*3.141592*2.) * 127.) * (1.-ds);
		float g =(128. - cos(ang + md*3.141592*2.) * 127.) * (1.-ds);
		float b =(128. - sin(ang + md*3.141592*2.) * 127.) * (1.-ds);

		gl_FragColor = vec4(r/255., g/255., b/255., 1.);
}
