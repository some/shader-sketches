#version 120

#define ORANGE vec3(222.,107.,40.)
#define TEAL vec3(67.,134.,150.)



vec3 RGB(vec3 color){
	return vec3(color.r/255., color.g/255, color.b/255);
}

uniform vec2 u_resolution;
uniform float u_time;

vec2 res = u_resolution;
float t = u_time;

float smootherStep(float st);
vec3 hsl2rgb( in vec3 c );
vec3 rgb2hsl( in vec3 c );

void main(){
	//vec2 uv = (gl_FragCoord.xy-0.5*res) / res.y;
	vec2 uv = gl_FragCoord.xy / res.xy;
	uv.y *= 5.;
	float row = floor(uv.y);
	vec3 col;
	col = vec3(1);
	if(row == 0){
		col = mix(RGB(ORANGE),RGB(TEAL),vec3(uv.x));
	}
	if(row == 1){
		float e = smoothstep(0.,1.,uv.x);
		col = RGB(ORANGE) + vec3(e) * (RGB(TEAL) - RGB(ORANGE));
	} 
	if(row == 2){
		float e = smootherStep(uv.x);
		col = RGB(ORANGE) + vec3(e) * (RGB(TEAL) - RGB(ORANGE));
	}
	if(row == 3){
		float e = smootherStep(uv.x);
		vec3 c1 = rgb2hsl(RGB(ORANGE));
		vec3 c2 = rgb2hsl(RGB(TEAL));
		col = c1 + vec3(e) * (c2 - c1);
		col = hsl2rgb(col);

	}


	

	gl_FragColor = vec4(col,1.);

}
//METHODS
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
float smootherStep(float st){
	return st * st * st * (st * (st * 6. - 15.) + 10.);
}
vec3 hsl2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

    return c.z + c.y * (rgb-0.5)*(1.0-abs(2.0*c.z-1.0));
}
vec3 rgb2hsl( in vec3 c ){
  float h = 0.0;
	float s = 0.0;
	float l = 0.0;
	float r = c.r;
	float g = c.g;
	float b = c.b;
	float cMin = min( r, min( g, b ) );
	float cMax = max( r, max( g, b ) );

	l = ( cMax + cMin ) / 2.0;
	if ( cMax > cMin ) {
		float cDelta = cMax - cMin;
        
        //s = l < .05 ? cDelta / ( cMax + cMin ) : cDelta / ( 2.0 - ( cMax + cMin ) ); Original
		s = l < .0 ? cDelta / ( cMax + cMin ) : cDelta / ( 2.0 - ( cMax + cMin ) );
        
		if ( r == cMax ) {
			h = ( g - b ) / cDelta;
		} else if ( g == cMax ) {
			h = 2.0 + ( b - r ) / cDelta;
		} else {
			h = 4.0 + ( r - g ) / cDelta;
		}

		if ( h < 0.0) {
			h += 6.0;
		}
		h = h / 6.0;
	}
	return vec3( h, s, l );
}