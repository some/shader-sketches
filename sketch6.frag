#version 120

#define ORANGE vec3(222.,107.,40.)
#define TEAL vec3(67.,134.,150.)

float smootherStep(float st);
vec3 hsl2rgb( in vec3 c );
vec3 rgb2hsl( in vec3 c );


vec3 RGB(vec3 color){
	return vec3(color.r/255., color.g/255, color.b/255);
}

vec3 SPOT(vec2 polarCoord, float width, vec3 col1, vec3 col2){
	float wa = width/360.;
	float start = 0.;
	float end = wa;
	float e1 = smoothstep(start,end,polarCoord.x*2.);
	float e2 = smoothstep(start,end,1.-polarCoord.x*2.+1);
	vec3 c1 = col1 + vec3(e1) * (col2 - col1);
	vec3 c2 = col1 + vec3(e2) * (col2 - col1);
	vec3 spotlight1 = (c1+c2)*0.5;

	return spotlight1;
}

uniform vec2 u_resolution;
uniform float u_time;

vec2 res = u_resolution;
float t = u_time;

void main(){
	vec2 uv = (gl_FragCoord.xy-0.5*res) / res.y;
	vec2 uv2 = uv;
	vec2 uv3 = uv;
	vec2 uv4 = uv;
	vec2 uv5 = uv;
	vec2 uv6 = uv;

	t *= 0.5;
	uv.x += sin(t) * 0.2;
	uv.y += cos(t) * 0.2;
	float a = degrees(atan(uv.y,uv.x))/360.;
	a += tan(cos(sin(t * 0.314) + t*0.45164));
	a = fract(a);
	float d = length(uv);
	vec2 puv = vec2(a,d);

	//MOVE SPOTLIGHT 2
	uv2.x += sin(t*0.4) * 0.214;
	uv2.y += cos(t*0.9) * 0.322;
	//convert to polar
	float a2 = degrees(atan(uv2.y,uv2.x))/360.;
	a2 += tan(sin(cos(t * 0.322) + t*0.2792));
	a2 = fract(a2);
	float d2 = length(uv2);
	vec2 puv2 = vec2(a2,d2);

	
	//MOVE SPOTLIGHT 2
	uv3.x += sin(t*0.777) * 0.240;
	uv3.y += cos(t*0.666) * 0.29194;
	//convert to polar
	float a3 = degrees(atan(uv3.y,uv3.x))/360.;
	a3 += tan(sin(cos(t * 0.322) + t*0.3792));
	a3 = fract(a3);
	float d3 = length(uv3); 
	vec2 puv3 = vec2(a3,d3);

	
	
	
	//MOVE SPOTLIGHT 2
	uv4.x += sin(t*1.777) * 0.240;
	uv4.y += cos(cos(t*0.686)) * 0.49194;
	//convert to polar
	float a4 = degrees(atan(uv4.y,uv4.x))/360.;
	a4 += tan(sin(cos(t * 0.322) + t*1.3792));
	a4 = fract(a4);
	float d4 = length(uv4); 
	vec2 puv4 = vec2(a4,d4);


	//MOVE SPOTLIGHT 2
	uv5.x += sin(t*0.797) * 0.340;
	uv5.y += cos(t*0.266) * 0.329194;
	//convert to polar
	float a5 = degrees(atan(uv5.y,uv5.x))/360.;
	a5 += tan(sin(cos(t * 0.722) + t*0.4892));
	a5 = fract(a5);
	float d5 = length(uv5); 
	vec2 puv5 = vec2(a5,d5);


	//MOVE SPOTLIGHT 2
	uv6.x += cos(sin(t*0.677) + t*0.41) * 0.740;
	uv6.y += cos(t*0.1) * 0.2194;
	//convert to polar
	float a6 = degrees(atan(uv6.y,uv6.x))/360.;
	a6 += tan(sin(cos(t * 0.491) + t*0.171));
	a6 = fract(a6);
	float d6 = length(uv6); 
	vec2 puv6 = vec2(a6,d6);



	
	//col = vec3(a);

	//width of arc in degrees and normalized
	vec3 orange = RGB(ORANGE);
	orange *= 2.5;


	vec3 s1 = SPOT(puv,120.,orange,RGB(TEAL));
	vec3 s2 = SPOT(puv2,120.,orange,RGB(ORANGE));
	vec3 s3 = SPOT(puv3,120.,orange,RGB(TEAL));
	vec3 s4 = SPOT(puv4,120.,orange,RGB(TEAL));
	vec3 s5 = SPOT(puv5,120.,orange,RGB(TEAL));
	vec3 s6 = SPOT(puv6,120.,orange,RGB(TEAL));

	vec3 colorOut;
	colorOut = (s1 + s2 + s3 + s4 + s5 + s6)/7.;


	gl_FragColor =  vec4(colorOut,1.);

}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

float smootherStep(float st){
	return st * st * st * (st * (st * 6. - 15.) + 10.);
	//then
	// start + eval * (stop - start);
}
vec3 hsl2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

    return c.z + c.y * (rgb-0.5)*(1.0-abs(2.0*c.z-1.0));
}
vec3 rgb2hsl( in vec3 c ){
  float h = 0.0;
	float s = 0.0;
	float l = 0.0;
	float r = c.r;
	float g = c.g;
	float b = c.b;
	float cMin = min( r, min( g, b ) );
	float cMax = max( r, max( g, b ) );

	l = ( cMax + cMin ) / 2.0;
	if ( cMax > cMin ) {
		float cDelta = cMax - cMin;
        
        //s = l < .05 ? cDelta / ( cMax + cMin ) : cDelta / ( 2.0 - ( cMax + cMin ) ); Original
		s = l < .0 ? cDelta / ( cMax + cMin ) : cDelta / ( 2.0 - ( cMax + cMin ) );
        
		if ( r == cMax ) {
			h = ( g - b ) / cDelta;
		} else if ( g == cMax ) {
			h = 2.0 + ( b - r ) / cDelta;
		} else {
			h = 4.0 + ( r - g ) / cDelta;
		}

		if ( h < 0.0) {
			h += 6.0;
		}
		h = h / 6.0;
	}
	return vec3( h, s, l );
}