#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 r = u_resolution;
float t = u_time;

float noise(vec2 p);
float fbm(vec2 p);

mat2 rot(float a){ return mat2(cos(a),sin(a),-sin(a),cos(a)); }
float pattern ( vec2 p){
		vec2 q = vec2( fbm(p), fbm(p + vec2(5.2,1.3)));

		vec2 r = vec2( fbm(p + 4.0 * q + vec2(t, 9.2)),
					   fbm(p + 4.0 * q + vec2( 8.3,2.8)));

		return fbm( p + 4.0 * r);
}

vec3 b1 = vec3( 167., 31., 34.) / 255.;
vec3 b2 = vec3( 237., 222., 189.) / 255.;
vec3 b3 = vec3( 88., 117., 88.) / 255.;
vec3 b4 = vec3( 72., 80., 34.) / 255.;
vec3 b5 = vec3( 42., 119., 113.) / 255.;
vec3 b6 = vec3(32., 107., 105.) / 255.;

void main(){
		vec2 uv = (gl_FragCoord.xy-2.*r)/r.y;
		uv*=rot(t*0.1*sin(t*0.01)*0.01*cos(t*0.01));
		uv+=vec2(sin(t*0.01),cos(t*0.1));
		uv *= 10.;

		vec2 q = vec2( fbm(uv), fbm(uv + vec2(5.2,t*0.1)));

		vec2 rbm = vec2( fbm(uv + 4.0 * q + vec2(t, 9.2)),
					   fbm(uv + 4.0 * q + vec2( 9.3,2.8)));


		float f = fbm(uv + 4. * rbm);
		//vec3 col = vec3(f);
		vec3 col = mix(b2,b6,f);
		col *= mix(col, b1, length(rbm));
		
				   //1.-vec3(0.73,0.466,0.53),
				   //length(q));

		gl_FragColor = vec4(col,1.);

}


//NOISE IMPLEMENTATION
//
float rand(vec2 n) { 
	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float noise(vec2 p){
		vec2 i = floor(p);
		vec2 f = fract(p);

		float a = rand(i);
		float b = rand(i + vec2(1.,0.) );
		float c = rand(i + vec2(0., 1.));
		float d = rand(i+ vec2(1.,1.));

		vec2 u = f * f * (3. - 2. * f);

		return mix(a,b,u.x) + (c-a) * u.y * (1. - u.x) + (d-b) * u.x * u.y;
}

mat2 m = mat2(0.8, 0.6, -0.6, 0.8);

#define OCTAVES 2

float fbm(vec2 p){
		float f = 0.0;
		float h = sin(t) *0.10 + 0.5  ; 

		for(int i = 0; i < OCTAVES; i ++){
			//f +=  sin(p.x);
			f += h * noise(p) ;
			p *= rot(t*0.0005) * vec2(2.02,0.1);
			h *= h;
		}

		return  f;
}
