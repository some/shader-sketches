#version 120

uniform vec2 u_resolution;
uniform float u_time;

vec2 res = u_resolution;
float t = u_time;

void main(){
	vec2 uv = (gl_FragCoord.xy-0.5*res) / res.y;
	vec3 col = vec3(uv.x,uv.y,0.);
	gl_FragColor = vec4(col,1.);

}